#!/usr/bin/env bash



set -eu -o pipefail

#scp tasmota.bin cubes.work:/var/www/195.201.35.8/
#scp tasmota-lite.bin  cubes.work:/var/www/195.201.35.8/
#sha256sum tasmota*

if [ $# -lt 2 ]
then
  echo
  echo "usage: $0 <device-id> <ip>"
  echo
  echo "Example:"
  echo " $0 1000c98765 192.168.1.99"
  echo
  exit 1
fi

dev="$1"
ip="$2"

echo "request info..."
timeout 15 curl http://$ip:8081/zeroconf/info -XPOST --data '{"deviceid":"'"$dev"'","data":{} }'
echo

echo "unlock?"
read
echo "unlocking..."
curl http://$ip:8081/zeroconf/ota_unlock -XPOST --data '{"deviceid":"<deviceID>","data":{} }'
echo

echo "request info..."
curl http://$ip:8081/zeroconf/info -XPOST --data '{"deviceid":"'"$dev"'","data":{} }'
echo

echo "flash firmware?"
read
echo "flashing..."
#curl http://$ip:8081/zeroconf/ota_flash -XPOST --data '{"deviceid":"'"$dev"'","data":{"downloadUrl": "http://195.201.35.8/tasmota.bin", "sha256sum": "12a03e7f486c2974a79f058f0a94cc063c8aa35550370e5e9e2764ec99bc341a"} }'
curl http://$ip:8081/zeroconf/ota_flash -XPOST --data '{"deviceid":"'"$dev"'","data":{"downloadUrl": "http://195.201.35.8/tasmota-lite.bin", "sha256sum": "bde52065ba4af5fc16c170321364d056d06961382f739d5d26a32a850b81ecf1"} }'
echo

echo done
